THISDIR="$(dirname "$(which "$0")")"
if [ "$#" -eq 3 ]; then
time gringo --output=SMODELS $1 $3 | ./$THISDIR/wasp.bin --interpreter=cpp --script-directory=. --plugins-file=$2
fi
if [ "$#" -eq 2 ]; then
./$THISDIR/wasp.bin --lp2cpp-datalog $1 $2
else
    echo -e "Illegal number of arguments; either run \n  ./run.sh encoding lazy_program instance \nor \n  ./run.sh stratified_encoding instance"
fi
