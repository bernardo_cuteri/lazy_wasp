#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Python
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/511e4115/CautiousReasoning.o \
	${OBJECTDIR}/_ext/511e4115/Clause.o \
	${OBJECTDIR}/_ext/511e4115/DependencyGraph.o \
	${OBJECTDIR}/_ext/511e4115/Enumeration.o \
	${OBJECTDIR}/_ext/511e4115/GUSData.o \
	${OBJECTDIR}/_ext/511e4115/Learning.o \
	${OBJECTDIR}/_ext/511e4115/Literal.o \
	${OBJECTDIR}/_ext/511e4115/PredicateMinimization.o \
	${OBJECTDIR}/_ext/511e4115/QueryInterface.o \
	${OBJECTDIR}/_ext/511e4115/ReasonForBinaryClauses.o \
	${OBJECTDIR}/_ext/511e4115/Satelite.o \
	${OBJECTDIR}/_ext/511e4115/Solver.o \
	${OBJECTDIR}/_ext/511e4115/WaspFacade.o \
	${OBJECTDIR}/_ext/52a27290/ExternalHeuristic.o \
	${OBJECTDIR}/_ext/52a27290/MinisatHeuristic.o \
	${OBJECTDIR}/_ext/a4bb7a50/Dimacs.o \
	${OBJECTDIR}/_ext/a4bb7a50/GringoNumericFormat.o \
	${OBJECTDIR}/_ext/a4bb7a50/Rule.o \
	${OBJECTDIR}/_ext/a4bb7a50/WeightConstraint.o \
	${OBJECTDIR}/_ext/12c9b3f3/Interpreter.o \
	${OBJECTDIR}/_ext/12c9b3f3/MyCppLazyInterpreter.o \
	${OBJECTDIR}/_ext/12c9b3f3/MyPerlInterpreter.o \
	${OBJECTDIR}/_ext/12c9b3f3/MyPythonInterpreter.o \
	${OBJECTDIR}/_ext/f7d215ef/CompilationManager.o \
	${OBJECTDIR}/_ext/6c9c0d4d/Buffer.o \
	${OBJECTDIR}/_ext/6c9c0d4d/InputDirector.o \
	${OBJECTDIR}/_ext/c9b7fe1f/Options.o \
	${OBJECTDIR}/_ext/c9b7fe1f/Utils.o \
	${OBJECTDIR}/_ext/f7d215ef/ExecutionManager.o \
	${OBJECTDIR}/_ext/f7d215ef/Executor.o \
	${OBJECTDIR}/_ext/f7d215ef/LazyConstraintImpl.o \
	${OBJECTDIR}/_ext/c40f54aa/AuxiliaryMap.o \
	${OBJECTDIR}/_ext/c40f54aa/Tuple.o \
	${OBJECTDIR}/_ext/a3a91b78/ArithmeticExpression.o \
	${OBJECTDIR}/_ext/a3a91b78/ArithmeticRelation.o \
	${OBJECTDIR}/_ext/a3a91b78/Atom.o \
	${OBJECTDIR}/_ext/a3a91b78/Literal.o \
	${OBJECTDIR}/_ext/a3a91b78/Program.o \
	${OBJECTDIR}/_ext/a3a91b78/Rule.o \
	${OBJECTDIR}/_ext/f1e44a90/AspCore2InstanceBuilder.o \
	${OBJECTDIR}/_ext/f1e44a90/AspCore2ProgramBuilder.o \
	${OBJECTDIR}/_ext/eefa7c91/ConstantsManager.o \
	${OBJECTDIR}/_ext/eefa7c91/GraphWithTarjanAlgorithm.o \
	${OBJECTDIR}/_ext/eefa7c91/Indentation.o \
	${OBJECTDIR}/_ext/eefa7c91/SharedFunctions.o \
	${OBJECTDIR}/_ext/511e4115/main.o \
	${OBJECTDIR}/_ext/ad784493/CompetitionOutputBuilder.o \
	${OBJECTDIR}/_ext/ad784493/DimacsOutputBuilder.o \
	${OBJECTDIR}/_ext/ad784493/IdOutputBuilder.o \
	${OBJECTDIR}/_ext/ad784493/MultiOutputBuilder.o \
	${OBJECTDIR}/_ext/ad784493/OutputBuilder.o \
	${OBJECTDIR}/_ext/ad784493/SilentOutputBuilder.o \
	${OBJECTDIR}/_ext/ad784493/ThirdCompetitionOutputBuilder.o \
	${OBJECTDIR}/_ext/ad784493/WaspOutputBuilder.o \
	${OBJECTDIR}/_ext/3ca0eaba/Aggregate.o \
	${OBJECTDIR}/_ext/3ca0eaba/CardinalityConstraint.o \
	${OBJECTDIR}/_ext/3ca0eaba/Component.o \
	${OBJECTDIR}/_ext/3ca0eaba/DisjunctionPropagator.o \
	${OBJECTDIR}/_ext/3ca0eaba/ExternalPropagator.o \
	${OBJECTDIR}/_ext/3ca0eaba/HCComponent.o \
	${OBJECTDIR}/_ext/3ca0eaba/MultiAggregate.o \
	${OBJECTDIR}/_ext/3ca0eaba/ReductBasedCheck.o \
	${OBJECTDIR}/_ext/3ca0eaba/UnfoundedBasedCheck.o \
	${OBJECTDIR}/_ext/c34564bc/ExtendedStatistics.o \
	${OBJECTDIR}/_ext/c34564bc/Statistics.o \
	${OBJECTDIR}/_ext/c34564bc/VariableNames.o \
	${OBJECTDIR}/_ext/c34564bc/WaspOptions.o \
	${OBJECTDIR}/_ext/96d35ec4/K.o \
	${OBJECTDIR}/_ext/96d35ec4/LazyInstantiation.o \
	${OBJECTDIR}/_ext/96d35ec4/Mgd.o \
	${OBJECTDIR}/_ext/96d35ec4/One.o \
	${OBJECTDIR}/_ext/96d35ec4/OneBB.o \
	${OBJECTDIR}/_ext/96d35ec4/Opt.o \
	${OBJECTDIR}/_ext/96d35ec4/OptimizationProblemUtils.o \
	${OBJECTDIR}/_ext/96d35ec4/PMRes.o \
	${OBJECTDIR}/_ext/96d35ec4/WeakInterface.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-DNDEBUG -DENABLE_PYTHON -I/usr/include/python2.7
CXXFLAGS=-DNDEBUG -DENABLE_PYTHON -I/usr/include/python2.7

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ../wasp.bin

../wasp.bin: ${OBJECTFILES}
	${MKDIR} -p ..
	${LINK.cc} -o ../wasp.bin ${OBJECTFILES} ${LDLIBSOPTIONS} -ldl -lpython2.7

${OBJECTDIR}/_ext/511e4115/CautiousReasoning.o: ../src/CautiousReasoning.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/CautiousReasoning.o ../src/CautiousReasoning.cpp

${OBJECTDIR}/_ext/511e4115/Clause.o: ../src/Clause.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/Clause.o ../src/Clause.cpp

${OBJECTDIR}/_ext/511e4115/DependencyGraph.o: ../src/DependencyGraph.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/DependencyGraph.o ../src/DependencyGraph.cpp

${OBJECTDIR}/_ext/511e4115/Enumeration.o: ../src/Enumeration.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/Enumeration.o ../src/Enumeration.cpp

${OBJECTDIR}/_ext/511e4115/GUSData.o: ../src/GUSData.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/GUSData.o ../src/GUSData.cpp

${OBJECTDIR}/_ext/511e4115/Learning.o: ../src/Learning.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/Learning.o ../src/Learning.cpp

${OBJECTDIR}/_ext/511e4115/Literal.o: ../src/Literal.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/Literal.o ../src/Literal.cpp

${OBJECTDIR}/_ext/511e4115/PredicateMinimization.o: ../src/PredicateMinimization.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/PredicateMinimization.o ../src/PredicateMinimization.cpp

${OBJECTDIR}/_ext/511e4115/QueryInterface.o: ../src/QueryInterface.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/QueryInterface.o ../src/QueryInterface.cpp

${OBJECTDIR}/_ext/511e4115/ReasonForBinaryClauses.o: ../src/ReasonForBinaryClauses.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/ReasonForBinaryClauses.o ../src/ReasonForBinaryClauses.cpp

${OBJECTDIR}/_ext/511e4115/Satelite.o: ../src/Satelite.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/Satelite.o ../src/Satelite.cpp

${OBJECTDIR}/_ext/511e4115/Solver.o: ../src/Solver.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/Solver.o ../src/Solver.cpp

${OBJECTDIR}/_ext/511e4115/WaspFacade.o: ../src/WaspFacade.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/WaspFacade.o ../src/WaspFacade.cpp

${OBJECTDIR}/_ext/52a27290/ExternalHeuristic.o: ../src/heuristic/ExternalHeuristic.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/52a27290
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/52a27290/ExternalHeuristic.o ../src/heuristic/ExternalHeuristic.cpp

${OBJECTDIR}/_ext/52a27290/MinisatHeuristic.o: ../src/heuristic/MinisatHeuristic.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/52a27290
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/52a27290/MinisatHeuristic.o ../src/heuristic/MinisatHeuristic.cpp

${OBJECTDIR}/_ext/a4bb7a50/Dimacs.o: ../src/input/Dimacs.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a4bb7a50
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a4bb7a50/Dimacs.o ../src/input/Dimacs.cpp

${OBJECTDIR}/_ext/a4bb7a50/GringoNumericFormat.o: ../src/input/GringoNumericFormat.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a4bb7a50
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a4bb7a50/GringoNumericFormat.o ../src/input/GringoNumericFormat.cpp

${OBJECTDIR}/_ext/a4bb7a50/Rule.o: ../src/input/Rule.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a4bb7a50
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a4bb7a50/Rule.o ../src/input/Rule.cpp

${OBJECTDIR}/_ext/a4bb7a50/WeightConstraint.o: ../src/input/WeightConstraint.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a4bb7a50
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a4bb7a50/WeightConstraint.o ../src/input/WeightConstraint.cpp

${OBJECTDIR}/_ext/12c9b3f3/Interpreter.o: ../src/interpreters/Interpreter.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/12c9b3f3
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/12c9b3f3/Interpreter.o ../src/interpreters/Interpreter.cpp

${OBJECTDIR}/_ext/12c9b3f3/MyCppLazyInterpreter.o: ../src/interpreters/MyCppLazyInterpreter.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/12c9b3f3
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/12c9b3f3/MyCppLazyInterpreter.o ../src/interpreters/MyCppLazyInterpreter.cpp

${OBJECTDIR}/_ext/12c9b3f3/MyPerlInterpreter.o: ../src/interpreters/MyPerlInterpreter.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/12c9b3f3
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/12c9b3f3/MyPerlInterpreter.o ../src/interpreters/MyPerlInterpreter.cpp

${OBJECTDIR}/_ext/12c9b3f3/MyPythonInterpreter.o: ../src/interpreters/MyPythonInterpreter.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/12c9b3f3
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/12c9b3f3/MyPythonInterpreter.o ../src/interpreters/MyPythonInterpreter.cpp

${OBJECTDIR}/_ext/f7d215ef/CompilationManager.o: ../src/lp2cpp/CompilationManager.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/f7d215ef
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f7d215ef/CompilationManager.o ../src/lp2cpp/CompilationManager.cpp

${OBJECTDIR}/_ext/6c9c0d4d/Buffer.o: ../src/lp2cpp/DLV2libs/input/Buffer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6c9c0d4d
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6c9c0d4d/Buffer.o ../src/lp2cpp/DLV2libs/input/Buffer.cpp

${OBJECTDIR}/_ext/6c9c0d4d/InputDirector.o: ../src/lp2cpp/DLV2libs/input/InputDirector.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6c9c0d4d
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6c9c0d4d/InputDirector.o ../src/lp2cpp/DLV2libs/input/InputDirector.cpp

${OBJECTDIR}/_ext/c9b7fe1f/Options.o: ../src/lp2cpp/DLV2libs/util/Options.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c9b7fe1f
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c9b7fe1f/Options.o ../src/lp2cpp/DLV2libs/util/Options.cpp

${OBJECTDIR}/_ext/c9b7fe1f/Utils.o: ../src/lp2cpp/DLV2libs/util/Utils.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c9b7fe1f
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c9b7fe1f/Utils.o ../src/lp2cpp/DLV2libs/util/Utils.cpp

${OBJECTDIR}/_ext/f7d215ef/ExecutionManager.o: ../src/lp2cpp/ExecutionManager.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/f7d215ef
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f7d215ef/ExecutionManager.o ../src/lp2cpp/ExecutionManager.cpp

${OBJECTDIR}/_ext/f7d215ef/Executor.o: ../src/lp2cpp/Executor.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/f7d215ef
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f7d215ef/Executor.o ../src/lp2cpp/Executor.cpp

${OBJECTDIR}/_ext/f7d215ef/LazyConstraintImpl.o: ../src/lp2cpp/LazyConstraintImpl.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/f7d215ef
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f7d215ef/LazyConstraintImpl.o ../src/lp2cpp/LazyConstraintImpl.cpp

${OBJECTDIR}/_ext/c40f54aa/AuxiliaryMap.o: ../src/lp2cpp/datastructures/AuxiliaryMap.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c40f54aa
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c40f54aa/AuxiliaryMap.o ../src/lp2cpp/datastructures/AuxiliaryMap.cpp

${OBJECTDIR}/_ext/c40f54aa/Tuple.o: ../src/lp2cpp/datastructures/Tuple.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c40f54aa
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c40f54aa/Tuple.o ../src/lp2cpp/datastructures/Tuple.cpp

${OBJECTDIR}/_ext/a3a91b78/ArithmeticExpression.o: ../src/lp2cpp/language/ArithmeticExpression.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a3a91b78
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a3a91b78/ArithmeticExpression.o ../src/lp2cpp/language/ArithmeticExpression.cpp

${OBJECTDIR}/_ext/a3a91b78/ArithmeticRelation.o: ../src/lp2cpp/language/ArithmeticRelation.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a3a91b78
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a3a91b78/ArithmeticRelation.o ../src/lp2cpp/language/ArithmeticRelation.cpp

${OBJECTDIR}/_ext/a3a91b78/Atom.o: ../src/lp2cpp/language/Atom.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a3a91b78
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a3a91b78/Atom.o ../src/lp2cpp/language/Atom.cpp

${OBJECTDIR}/_ext/a3a91b78/Literal.o: ../src/lp2cpp/language/Literal.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a3a91b78
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a3a91b78/Literal.o ../src/lp2cpp/language/Literal.cpp

${OBJECTDIR}/_ext/a3a91b78/Program.o: ../src/lp2cpp/language/Program.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a3a91b78
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a3a91b78/Program.o ../src/lp2cpp/language/Program.cpp

${OBJECTDIR}/_ext/a3a91b78/Rule.o: ../src/lp2cpp/language/Rule.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a3a91b78
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a3a91b78/Rule.o ../src/lp2cpp/language/Rule.cpp

${OBJECTDIR}/_ext/f1e44a90/AspCore2InstanceBuilder.o: ../src/lp2cpp/parsing/AspCore2InstanceBuilder.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/f1e44a90
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f1e44a90/AspCore2InstanceBuilder.o ../src/lp2cpp/parsing/AspCore2InstanceBuilder.cpp

${OBJECTDIR}/_ext/f1e44a90/AspCore2ProgramBuilder.o: ../src/lp2cpp/parsing/AspCore2ProgramBuilder.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/f1e44a90
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f1e44a90/AspCore2ProgramBuilder.o ../src/lp2cpp/parsing/AspCore2ProgramBuilder.cpp

${OBJECTDIR}/_ext/eefa7c91/ConstantsManager.o: ../src/lp2cpp/utils/ConstantsManager.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/eefa7c91
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/eefa7c91/ConstantsManager.o ../src/lp2cpp/utils/ConstantsManager.cpp

${OBJECTDIR}/_ext/eefa7c91/GraphWithTarjanAlgorithm.o: ../src/lp2cpp/utils/GraphWithTarjanAlgorithm.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/eefa7c91
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/eefa7c91/GraphWithTarjanAlgorithm.o ../src/lp2cpp/utils/GraphWithTarjanAlgorithm.cpp

${OBJECTDIR}/_ext/eefa7c91/Indentation.o: ../src/lp2cpp/utils/Indentation.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/eefa7c91
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/eefa7c91/Indentation.o ../src/lp2cpp/utils/Indentation.cpp

${OBJECTDIR}/_ext/eefa7c91/SharedFunctions.o: ../src/lp2cpp/utils/SharedFunctions.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/eefa7c91
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/eefa7c91/SharedFunctions.o ../src/lp2cpp/utils/SharedFunctions.cpp

${OBJECTDIR}/_ext/511e4115/main.o: ../src/main.cc
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/main.o ../src/main.cc

${OBJECTDIR}/_ext/ad784493/CompetitionOutputBuilder.o: ../src/outputBuilders/CompetitionOutputBuilder.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/ad784493
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ad784493/CompetitionOutputBuilder.o ../src/outputBuilders/CompetitionOutputBuilder.cpp

${OBJECTDIR}/_ext/ad784493/DimacsOutputBuilder.o: ../src/outputBuilders/DimacsOutputBuilder.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/ad784493
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ad784493/DimacsOutputBuilder.o ../src/outputBuilders/DimacsOutputBuilder.cpp

${OBJECTDIR}/_ext/ad784493/IdOutputBuilder.o: ../src/outputBuilders/IdOutputBuilder.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/ad784493
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ad784493/IdOutputBuilder.o ../src/outputBuilders/IdOutputBuilder.cpp

${OBJECTDIR}/_ext/ad784493/MultiOutputBuilder.o: ../src/outputBuilders/MultiOutputBuilder.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/ad784493
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ad784493/MultiOutputBuilder.o ../src/outputBuilders/MultiOutputBuilder.cpp

${OBJECTDIR}/_ext/ad784493/OutputBuilder.o: ../src/outputBuilders/OutputBuilder.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/ad784493
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ad784493/OutputBuilder.o ../src/outputBuilders/OutputBuilder.cpp

${OBJECTDIR}/_ext/ad784493/SilentOutputBuilder.o: ../src/outputBuilders/SilentOutputBuilder.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/ad784493
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ad784493/SilentOutputBuilder.o ../src/outputBuilders/SilentOutputBuilder.cpp

${OBJECTDIR}/_ext/ad784493/ThirdCompetitionOutputBuilder.o: ../src/outputBuilders/ThirdCompetitionOutputBuilder.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/ad784493
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ad784493/ThirdCompetitionOutputBuilder.o ../src/outputBuilders/ThirdCompetitionOutputBuilder.cpp

${OBJECTDIR}/_ext/ad784493/WaspOutputBuilder.o: ../src/outputBuilders/WaspOutputBuilder.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/ad784493
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ad784493/WaspOutputBuilder.o ../src/outputBuilders/WaspOutputBuilder.cpp

${OBJECTDIR}/_ext/3ca0eaba/Aggregate.o: ../src/propagators/Aggregate.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/3ca0eaba
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/3ca0eaba/Aggregate.o ../src/propagators/Aggregate.cpp

${OBJECTDIR}/_ext/3ca0eaba/CardinalityConstraint.o: ../src/propagators/CardinalityConstraint.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/3ca0eaba
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/3ca0eaba/CardinalityConstraint.o ../src/propagators/CardinalityConstraint.cpp

${OBJECTDIR}/_ext/3ca0eaba/Component.o: ../src/propagators/Component.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/3ca0eaba
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/3ca0eaba/Component.o ../src/propagators/Component.cpp

${OBJECTDIR}/_ext/3ca0eaba/DisjunctionPropagator.o: ../src/propagators/DisjunctionPropagator.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/3ca0eaba
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/3ca0eaba/DisjunctionPropagator.o ../src/propagators/DisjunctionPropagator.cpp

${OBJECTDIR}/_ext/3ca0eaba/ExternalPropagator.o: ../src/propagators/ExternalPropagator.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/3ca0eaba
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/3ca0eaba/ExternalPropagator.o ../src/propagators/ExternalPropagator.cpp

${OBJECTDIR}/_ext/3ca0eaba/HCComponent.o: ../src/propagators/HCComponent.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/3ca0eaba
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/3ca0eaba/HCComponent.o ../src/propagators/HCComponent.cpp

${OBJECTDIR}/_ext/3ca0eaba/MultiAggregate.o: ../src/propagators/MultiAggregate.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/3ca0eaba
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/3ca0eaba/MultiAggregate.o ../src/propagators/MultiAggregate.cpp

${OBJECTDIR}/_ext/3ca0eaba/ReductBasedCheck.o: ../src/propagators/ReductBasedCheck.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/3ca0eaba
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/3ca0eaba/ReductBasedCheck.o ../src/propagators/ReductBasedCheck.cpp

${OBJECTDIR}/_ext/3ca0eaba/UnfoundedBasedCheck.o: ../src/propagators/UnfoundedBasedCheck.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/3ca0eaba
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/3ca0eaba/UnfoundedBasedCheck.o ../src/propagators/UnfoundedBasedCheck.cpp

${OBJECTDIR}/_ext/c34564bc/ExtendedStatistics.o: ../src/util/ExtendedStatistics.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c34564bc
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c34564bc/ExtendedStatistics.o ../src/util/ExtendedStatistics.cpp

${OBJECTDIR}/_ext/c34564bc/Statistics.o: ../src/util/Statistics.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c34564bc
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c34564bc/Statistics.o ../src/util/Statistics.cpp

${OBJECTDIR}/_ext/c34564bc/VariableNames.o: ../src/util/VariableNames.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c34564bc
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c34564bc/VariableNames.o ../src/util/VariableNames.cpp

${OBJECTDIR}/_ext/c34564bc/WaspOptions.o: ../src/util/WaspOptions.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/c34564bc
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c34564bc/WaspOptions.o ../src/util/WaspOptions.cpp

${OBJECTDIR}/_ext/96d35ec4/K.o: ../src/weakconstraints/K.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/96d35ec4
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/96d35ec4/K.o ../src/weakconstraints/K.cpp

${OBJECTDIR}/_ext/96d35ec4/LazyInstantiation.o: ../src/weakconstraints/LazyInstantiation.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/96d35ec4
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/96d35ec4/LazyInstantiation.o ../src/weakconstraints/LazyInstantiation.cpp

${OBJECTDIR}/_ext/96d35ec4/Mgd.o: ../src/weakconstraints/Mgd.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/96d35ec4
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/96d35ec4/Mgd.o ../src/weakconstraints/Mgd.cpp

${OBJECTDIR}/_ext/96d35ec4/One.o: ../src/weakconstraints/One.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/96d35ec4
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/96d35ec4/One.o ../src/weakconstraints/One.cpp

${OBJECTDIR}/_ext/96d35ec4/OneBB.o: ../src/weakconstraints/OneBB.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/96d35ec4
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/96d35ec4/OneBB.o ../src/weakconstraints/OneBB.cpp

${OBJECTDIR}/_ext/96d35ec4/Opt.o: ../src/weakconstraints/Opt.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/96d35ec4
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/96d35ec4/Opt.o ../src/weakconstraints/Opt.cpp

${OBJECTDIR}/_ext/96d35ec4/OptimizationProblemUtils.o: ../src/weakconstraints/OptimizationProblemUtils.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/96d35ec4
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/96d35ec4/OptimizationProblemUtils.o ../src/weakconstraints/OptimizationProblemUtils.cpp

${OBJECTDIR}/_ext/96d35ec4/PMRes.o: ../src/weakconstraints/PMRes.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/96d35ec4
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/96d35ec4/PMRes.o ../src/weakconstraints/PMRes.cpp

${OBJECTDIR}/_ext/96d35ec4/WeakInterface.o: ../src/weakconstraints/WeakInterface.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/96d35ec4
	${RM} "$@.d"
	$(COMPILE.cc) -O3 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/96d35ec4/WeakInterface.o ../src/weakconstraints/WeakInterface.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
