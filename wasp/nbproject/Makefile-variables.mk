#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux
CND_ARTIFACT_DIR_Debug=..
CND_ARTIFACT_NAME_Debug=wasp.bin
CND_ARTIFACT_PATH_Debug=../wasp.bin
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux/package
CND_PACKAGE_NAME_Debug=wasp.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux/package/wasp.tar
# StaticExecutor configuration
CND_PLATFORM_StaticExecutor=GNU-Linux
CND_ARTIFACT_DIR_StaticExecutor=..
CND_ARTIFACT_NAME_StaticExecutor=wasp.bin
CND_ARTIFACT_PATH_StaticExecutor=../wasp.bin
CND_PACKAGE_DIR_StaticExecutor=dist/StaticExecutor/GNU-Linux/package
CND_PACKAGE_NAME_StaticExecutor=wasp.tar
CND_PACKAGE_PATH_StaticExecutor=dist/StaticExecutor/GNU-Linux/package/wasp.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux
CND_ARTIFACT_NAME_Release=wasp
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux/wasp
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux/package
CND_PACKAGE_NAME_Release=wasp.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux/package/wasp.tar
# Python configuration
CND_PLATFORM_Python=GNU-Linux
CND_ARTIFACT_DIR_Python=..
CND_ARTIFACT_NAME_Python=wasp.bin
CND_ARTIFACT_PATH_Python=../wasp.bin
CND_PACKAGE_DIR_Python=dist/Python/GNU-Linux/package
CND_PACKAGE_NAME_Python=wasp.tar
CND_PACKAGE_PATH_Python=dist/Python/GNU-Linux/package/wasp.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
